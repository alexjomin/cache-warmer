package main

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// ChangeFreq specifies change frequency of a sitemap entry. It is just a string.
type ChangeFreq string

// Feel free to use these constants for ChangeFreq (or you can just supply
// a string directly).
const (
	Always  ChangeFreq = "always"
	Hourly  ChangeFreq = "hourly"
	Daily   ChangeFreq = "daily"
	Weekly  ChangeFreq = "weekly"
	Monthly ChangeFreq = "monthly"
	Yearly  ChangeFreq = "yearly"
	Never   ChangeFreq = "never"
)

// URL entry in sitemap or sitemap index. LastMod is a pointer
// to time.Time because omitempty does not work otherwise. Loc is the
// only mandatory item. ChangeFreq and Priority must be left empty when
// using with a sitemap index.
type URL struct {
	Loc        string     `xml:"loc"`
	LastMod    *time.Time `xml:"lastmod,omitempty"`
	ChangeFreq ChangeFreq `xml:"changefreq,omitempty"`
	Priority   float32    `xml:"priority,omitempty"`
}

// Sitemap represents a complete sitemap which can be marshaled to XML.
// New instances must be created with New() in order to set the xmlns
// attribute correctly. Minify can be set to make the output less human
// readable.
type Sitemap struct {
	XMLName xml.Name `xml:"urlset"`
	Xmlns   string   `xml:"xmlns,attr"`
	URLs    []*URL   `xml:"url"`
	Minify  bool     `xml:"-"`
}

// New returns a new Sitemap.
func New() *Sitemap {
	return &Sitemap{
		Xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9",
		URLs:  make([]*URL, 0),
	}
}

func main() {
	resp, err := http.Get("http://v3.la-trace.com/sitemap")

	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		panic(err)
	}

	sitemap := New()
	xml.Unmarshal(body, &sitemap)

	for _, u := range sitemap.URLs {
		r, _ := http.NewRequest(http.MethodGet, u.Loc, nil)
		r.Header.Set("Cache-Control", "must-revalidate")
		r.Header.Set("Accept-Language", "fr")
		r.Header.Set("User-Agent", "Googlebot/2.1 (+http://www.google.com/bot.html)")
		c := http.Client{
			Timeout: time.Second * 20,
		}
		response, err := c.Do(r)

		if err != nil {
			log.Printf("Response err - %s", err)
			continue
		}

		log.Printf("%d - %s\n", response.StatusCode, u.Loc)
	}

}
